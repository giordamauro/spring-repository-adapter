/*
 * Copyright 2015-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mgiorda.repositoryadapter;

import com.mgiorda.repositoryadapter.adapted.model.NewCompany;
import com.mgiorda.repositoryadapter.adapted.repository.NewCompanyRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class})
@EnableTransactionManagement
public class RepositoryAdapterApplication {

    public static void main(String[] args) {
        SpringApplication.run(RepositoryAdapterApplication.class, args);
    }

    @Bean
    public CommandLineRunner loadData(NewCompanyRepository newCompanyRepository) {
        return (args) -> {
            // save a couple of customers
            newCompanyRepository.save(new NewCompany("Jack"));

            // fetch all customers
            for (NewCompany company : newCompanyRepository.findAll()) {
                System.out.println(company);
            }
        };
    }
}
