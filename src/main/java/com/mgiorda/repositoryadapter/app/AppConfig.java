package com.mgiorda.repositoryadapter.app;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

import java.util.Properties;

import static com.mgiorda.repositoryadapter.app.AppConfig.APP_TRANSACTION_MANAGER;

@Configuration
@EnableCaching
@EnableJpaRepositories(entityManagerFactoryRef = "appEntityManagerFactory",
        transactionManagerRef = APP_TRANSACTION_MANAGER)
public class AppConfig {

    public static final String APP_TRANSACTION_MANAGER = "appTransactionManager";

    @Bean
//    @ConfigurationProperties(prefix = "app.datasource")
    DataSource appDataSource() {

        return new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.HSQL)
                .setName("base-app")
                .build();
    }

    @Primary
    @Bean
    PlatformTransactionManager appTransactionManager() {
        return new JpaTransactionManager(appEntityManagerFactory().getObject());
    }

    @Bean
    LocalContainerEntityManagerFactoryBean appEntityManagerFactory() {

        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setGenerateDdl(true);
//        jpaVendorAdapter.setShowSql(true);

        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();

        factoryBean.setDataSource(appDataSource());
        factoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        factoryBean.setPackagesToScan(AppConfig.class.getPackage().getName());

        Properties properties = new Properties();
        properties.put("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");

        factoryBean.setJpaProperties(properties);

        return factoryBean;
    }
}
