package com.mgiorda.repositoryadapter.app.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor(force = true)
@ToString(exclude = "company")

@Entity
public class UserGroup {

    @Id
    @GeneratedValue
    private final Integer id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "company_id")
    private Company company;

    @NotNull
    private String name;

    @NotNull
    private String type;

    private Boolean active = true;
}