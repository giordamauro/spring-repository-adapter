package com.mgiorda.repositoryadapter.app.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor(force = true)

@Entity
public class Company {

    @Id
    @GeneratedValue
    private final Integer id;

    @NotNull
    @Column(unique = true)
    private String name;

    @NotNull
    private String displayName;

    private Boolean active = true;
}
