package com.mgiorda.repositoryadapter.app.repository;

import com.mgiorda.repositoryadapter.app.model.UserGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

public interface UserGroupRepository extends JpaRepository<UserGroup, Integer>, QueryDslPredicateExecutor<UserGroup> {

    @Query("SELECT e.id FROM UserGroup e WHERE e.name = :groupName AND e.company.id = :companyId" )
    Integer findIdByNameAndCompanyId(@Param("groupName") String groupName, @Param("companyId") Integer companyId);
}
