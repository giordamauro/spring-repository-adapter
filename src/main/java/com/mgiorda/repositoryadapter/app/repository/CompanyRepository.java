package com.mgiorda.repositoryadapter.app.repository;

import com.mgiorda.repositoryadapter.app.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.query.Param;

public interface CompanyRepository extends JpaRepository<Company, Integer>, QueryDslPredicateExecutor<Company> {

    @Query("SELECT e.id FROM Company e WHERE e.name = :name" )
    Integer findIdByName(@Param("name") String name);
}
