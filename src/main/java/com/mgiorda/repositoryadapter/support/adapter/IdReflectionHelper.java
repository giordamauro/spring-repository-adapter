package com.mgiorda.repositoryadapter.support.adapter;

import lombok.NonNull;
import org.springframework.util.ReflectionUtils;

import javax.persistence.EmbeddedId;
import javax.persistence.Id;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class IdReflectionHelper<E, ID extends Serializable> {

    private static final String ID_FIELD_NAME = "id";
    private final Field idField;

    public IdReflectionHelper(@NonNull Class<E> jpaClass) {

        this.idField = IdReflectionHelper.findJpaIdField(jpaClass);

        ReflectionUtils.makeAccessible(idField);
    }

    public ID getId(@NonNull E entity) {

        @SuppressWarnings("unchecked")
        ID idValue = (ID) ReflectionUtils.getField(idField, entity);

        return idValue;
    }

    public void setId(@NonNull E entity, @NonNull ID id) {

        ReflectionUtils.setField(idField, entity, id);
    }

    public static Field findJpaIdField(@NonNull Class<?> jpaClass) {

        Field idField = ReflectionUtils.findField(jpaClass, ID_FIELD_NAME);
        if (idField == null) {

            Set<Field> idFields = getAnnotatedFields(jpaClass, Id.class);

            if (idFields.isEmpty()) {
                idFields = getAnnotatedFields(jpaClass, EmbeddedId.class);
            }

            if (idFields.isEmpty()) {
                throw new IllegalStateException("Entity ID not found");
            }

            if (idFields.size() > 1) {
                throw new IllegalStateException("Unsupported multiple @Id columns - Consider using a @EmbeddedId");
            }

            idField = idFields.iterator().next();
        }

        return idField;
    }

    private static Set<Field> getAnnotatedFields(Class clazz, Class<? extends Annotation> annotationClass) {

        Set<Field> annotatedFields = new HashSet<>();

        for (Field field : getDeclaredFields(clazz)) {

            if(field.isAnnotationPresent(annotationClass)) {
                annotatedFields.add(field);
            }
        }

        return annotatedFields;
    }

    private static Set<Field> getDeclaredFields(Class clazz) {

        Set<Field> fields = new HashSet<>();

        Field[] declaredFields = clazz.getDeclaredFields();
        Collections.addAll(fields, declaredFields);

        Class superClass = clazz.getSuperclass();

        if(superClass != null) {
            Set<Field> declaredFieldsOfSuper = getDeclaredFields(superClass);
            fields.addAll(declaredFieldsOfSuper);
        }

        return fields;
    }

}
