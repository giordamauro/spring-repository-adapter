package com.mgiorda.repositoryadapter.support.adapter;

public interface ListEntityAdapter<BT, T, BE, E> {

    E createEntity(BE baseEntity, BT baseDto, T dto);

    void updateEntity(E entity, T dto);

    T createDto(BE baseEntity, E entity);

    void updateDto(T dto, E entity);
}
