package com.mgiorda.repositoryadapter.support.adapter;

import java.io.Serializable;

public interface IdAdapter<TID extends Serializable, E, ID extends Serializable> {

    TID getDtoId(E entity, ID entityId);

    ID getEntityId(TID dtoId);
}
