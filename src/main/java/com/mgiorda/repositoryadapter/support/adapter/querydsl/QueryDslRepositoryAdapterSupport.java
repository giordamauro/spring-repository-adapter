package com.mgiorda.repositoryadapter.support.adapter.querydsl;

import com.mgiorda.repositoryadapter.app.AppConfig;
import com.mgiorda.repositoryadapter.support.adapter.RepositoryAdapterSupport;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.PathBuilder;
import com.mgiorda.repositoryadapter.support.adapter.EntityAdapter;
import com.mgiorda.repositoryadapter.support.adapter.IdAdapter;
import lombok.NonNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collections;

@Transactional(AppConfig.APP_TRANSACTION_MANAGER)
public abstract class QueryDslRepositoryAdapterSupport<T, TID extends Serializable, E, EID extends Serializable> extends RepositoryAdapterSupport<T, TID, E, EID> implements QueryDslPredicateExecutor<T> {

    private final QueryDslPredicateExecutor<E> queryDslRepository;
    private final PathBuilder basePath;

    private QueryDslAdapterReplaceVisitor basePathVisitor;

    @SuppressWarnings("unchecked")
    protected <S extends QueryDslPredicateExecutor<E> & JpaRepository<E, EID>> QueryDslRepositoryAdapterSupport(
            @NonNull S repository, @NonNull EntityAdapter<T, E> entityAdapter, @NonNull IdAdapter<TID, E, EID> idAdapter) {
        super(repository, entityAdapter, idAdapter);

        this.queryDslRepository = repository;

        ParameterizedType paramType = (ParameterizedType) this.getClass()
                .getGenericSuperclass();
        Class<E> entityType = (Class) paramType.getActualTypeArguments()[1];

        this.basePath = new PathBuilder<>(entityType, deCapitalize(entityType.getSimpleName()));

        this.basePathVisitor = new QueryDslAdapterReplaceVisitor(basePath, Collections.emptyMap(), Collections.emptyMap());
    }

    protected Predicate adaptPredicate(Predicate source) {

        if(source == null){
            return null;
        }

        return (Predicate) source.accept(basePathVisitor, null);
    }

    public void setMappingsAdapter(@NonNull QueryDslMappingsAdapter mappingsAdapter) {
        this.basePathVisitor = new QueryDslAdapterReplaceVisitor(basePath, mappingsAdapter.getKeyMappings(), mappingsAdapter.getValueMappings());
    }

    @Override
    //@Cacheable(value="repoAdapterCache", keyGenerator="queryDslAdapterKeyGenerator")
    public Page<T> findAll(Predicate predicate, Pageable pageable) {

        Predicate newPredicate = this.adaptPredicate(predicate);
        Page<E> page = queryDslRepository.findAll(newPredicate, pageable);
        return page.map(this::readEntity);
    }

    @Override
    public Iterable<T> findAll(Predicate predicate) {
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public long count(Predicate predicate) {
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public Iterable<T> findAll(Predicate predicate, Sort sort) {
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public Iterable<T> findAll(Predicate predicate, OrderSpecifier<?>[] orders) {
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public Iterable<T> findAll(OrderSpecifier<?>[] orders) {
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public T findOne(Predicate predicate) {
        throw new UnsupportedOperationException("Not supported yet");
    }

    @Override
    public boolean exists(Predicate predicate) {
        throw new UnsupportedOperationException("Not supported yet");
    }

    private static String deCapitalize(String string) {

        if (string == null || string.length() == 0) {
            return string;
        }
        char[] c = string.toCharArray();
        c[0] = Character.toLowerCase(c[0]);

        return new String(c);
    }
}