package com.mgiorda.repositoryadapter.support.adapter;

import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Optional;

@Component
public class RepositoryAdapterKeyGenerator implements KeyGenerator {

    @Override
    public Object generate(Object target, Method method, Object... params) {

        StringBuilder key = new StringBuilder();

        key.append(target.getClass().getName());
//        key.append(method.getName());

        if(params != null) {

            for (Object param : params) {

                String paramKey = Optional.ofNullable(param)
                                          .map(Object::hashCode)
                                          .map(String::valueOf)
                                          .orElse("");
                key.append(paramKey);
            }
        }

        return key;
    }
}