package com.mgiorda.repositoryadapter.support.adapter.querydsl;

import java.util.Collections;
import java.util.Map;
import java.util.function.Function;

public interface QueryDslMappingsAdapter {

    default Map<String, Function<String, String>> getKeyMappings() {
        return Collections.emptyMap();
    }

    default Map<String, Function<Object, Object>> getValueMappings() {
        return Collections.emptyMap();
    }
}
