package com.mgiorda.repositoryadapter.support.adapter.querydsl;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import com.querydsl.core.support.ReplaceVisitor;
import com.querydsl.core.types.Constant;
import com.querydsl.core.types.ConstantImpl;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Operation;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.PathMetadata;
import com.querydsl.core.types.Predicate;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

@RequiredArgsConstructor
class QueryDslAdapterReplaceVisitor extends ReplaceVisitor<Void> {

        @NonNull
        private final Path basePath;

        @NonNull
        private final Map<String, Function<String, String>> keyMappings;

        @NonNull
        private final Map<String, Function<Object, Object>> valueMappings;

        @Override
        public Expression<?> visit(Path<?> expr, Void context) {
            if (expr.getMetadata().isRoot()) {
                return basePath;
            } else {
                PathMetadata metadata = expr.getMetadata();
                Path<?> parent = (Path) metadata.getParent().accept(this, context);
                Object element = metadata.getElement();
                if (element instanceof Expression<?>) {
                    element = ((Expression<?>) element).accept(this, context);
                }
                else if(element instanceof String && keyMappings.containsKey(element)){
                    element = keyMappings.get(element).apply((String) element);
                }
                if (parent.equals(metadata.getParent()) && Objects.equal(element, metadata.getElement())) {
                    return expr;
                } else {
                    metadata = new PathMetadata(parent, element, metadata.getPathType());
                    return ExpressionUtils.path(expr.getType(), metadata);
                }
            }
        }

        @Override
        public Expression<?> visit(Operation<?> expr, Void context) {
            ImmutableList<Expression<?>> args = visit(expr.getArgs(), context);
            if (args.equals(expr.getArgs())) {
                return expr;
            } else if (expr instanceof Predicate) {
                return ExpressionUtils.predicate(expr.getOperator(), args);
            } else {
                return ExpressionUtils.operation(expr.getType(), expr.getOperator(), args);
            }
        }

        private ImmutableList<Expression<?>> visit(List<Expression<?>> args, Void context) {
            ImmutableList.Builder<Expression<?>> builder = ImmutableList.builder();

            if(args.size() == 2 && args.get(0) instanceof Path && args.get(1) instanceof Constant) {

                Path<?> keyPath = (Path<?>) args.get(0);
                Constant constant = (Constant) args.get(1);

                PathMetadata metadata = keyPath.getMetadata();
                Object element = metadata.getElement();

                if(element instanceof String && valueMappings.containsKey(element)){
                    Object value = valueMappings.get(element).apply(constant.getConstant());
                    constant = ConstantImpl.create(value);
                }

                builder.add(keyPath.accept(this, context));
                builder.add(constant);
            }
            else {
                for (Expression<?> arg : args) {
                    builder.add(arg.accept(this, context));
                }
            }
            return builder.build();
        }
    }