package com.mgiorda.repositoryadapter.support.adapter;

import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.mgiorda.repositoryadapter.app.AppConfig.APP_TRANSACTION_MANAGER;

@Transactional(APP_TRANSACTION_MANAGER)
public abstract class RepositoryAdapterSupport<T, TID extends Serializable, E, EID extends Serializable> implements PagingAndSortingRepository<T, TID> {

    private final Logger jdbcLogger = LoggerFactory.getLogger("jdbc.custom");

    private final JpaRepository<E, EID> repository;
    private final EntityAdapter<T, E> entityAdapter;
    private final IdAdapter<TID, E, EID> idAdapter;

    private final IdReflectionHelper<T, TID> dtoIdHelper;
    private final IdReflectionHelper<E, EID> entityIdHelper;

    private final Map<TID, E> entitiesById = new HashMap<>();
    private final Map<EID, T> dtosByEntityId = new HashMap<>();

    protected RepositoryAdapterSupport(@NonNull JpaRepository<E, EID> repository, @NonNull EntityAdapter<T, E> entityAdapter, @NonNull IdAdapter<TID, E, EID> idAdapter) {

        this.repository =  repository;
        this.entityAdapter = entityAdapter;
        this.idAdapter = idAdapter;

        ParameterizedType paramType = (ParameterizedType) entityAdapter.getClass().getGenericInterfaces()[0];

        @SuppressWarnings("unchecked")
        Class<T> dtoType = (Class<T>) paramType.getActualTypeArguments()[0];
        @SuppressWarnings("unchecked")
        Class<E> entityType = (Class<E>) paramType.getActualTypeArguments()[1];

        this.dtoIdHelper = new IdReflectionHelper<>(dtoType);
        this.entityIdHelper = new IdReflectionHelper<>(entityType);
    }

    @Override
    @SuppressWarnings("unchecked")
    //@CacheEvict(value="repoAdapterCache", allEntries = true)
    public <S extends T> S save(@NonNull S dto) {

        TID dtoId = dtoIdHelper.getId(dto);

        E result = entitiesById.get(dtoId);

        if(result != null) {
            if (jdbcLogger.isInfoEnabled()) {
                jdbcLogger.info("-- " + result.toString());
            }
        }
        else {
            result = entityAdapter.createEntity(dto);
        }

        entityAdapter.updateEntity(result, dto);
        E persistedResult = repository.saveAndFlush(result);

        return (S) this.readEntity(persistedResult);
    }

    protected T readEntity(E entity) {

        EID entityId = entityIdHelper.getId(entity);

        T dto = dtosByEntityId.get(entityId);
        if(dto == null) {

            dto = entityAdapter.createDto(entity);
            TID dtoId = idAdapter.getDtoId(entity, entityId);

            dtoIdHelper.setId(dto, dtoId);
            dtosByEntityId.put(entityId, dto);
            entitiesById.put(dtoId, entity);
        }
        entityAdapter.updateDto(dto, entity);

        return dto;
    }

    @Override
    //@Cacheable(value="repoAdapterCache", keyGenerator="repositoryAdapterKeyGenerator")
    public List<T> findAll(Sort sort) {

        List<E> sortedCollection = repository.findAll(sort);

        return sortedCollection.stream()
                .map(this::readEntity)
                .collect(Collectors.toList());
    }

    @Override
    //@Cacheable(value="repoAdapterCache", keyGenerator="repositoryAdapterKeyGenerator")
    public Page<T> findAll(Pageable pageable) {

        Page<E> page = repository.findAll(pageable);

        return page.map(this::readEntity);
    }

    @Override
    //@Cacheable(value="repoAdapterCache", keyGenerator="repositoryAdapterKeyGenerator")
    public List<T> findAll() {

        List<E> list = repository.findAll();

        return list.stream()
                .map(this::readEntity)
                .collect(Collectors.toList());
    }

    @Override
    //@Cacheable(value="repoAdapterCache", keyGenerator="repositoryAdapterKeyGenerator")
    public List<T> findAll(Iterable<TID> iterable) {

        List<EID> ids = StreamSupport.stream(iterable.spliterator(), false)
                                     .map(idAdapter::getEntityId)
                                     .collect(Collectors.toList());

        List<E> sortedCollection = repository.findAll(ids);

        return sortedCollection.stream()
                .map(this::readEntity)
                .collect(Collectors.toList());
    }

    @Override
    public <S extends T> List<S> save(Iterable<S> iterable) {

        return StreamSupport.stream(iterable.spliterator(), false)
                .map(this::save)
                .collect(Collectors.toList());
    }

    @Override
    //@Cacheable(value="repoAdapterCache", keyGenerator="repositoryAdapterKeyGenerator")
    public T findOne(TID dtoId) {

        EID entityId = idAdapter.getEntityId(dtoId);

        E entity = repository.findOne(entityId);

        return Optional.ofNullable(entity)
                .map(this::readEntity)
                .orElse(null);
    }

    @Override
    public boolean exists(TID dtoId) {

        EID entityId = idAdapter.getEntityId(dtoId);

        return repository.exists(entityId);
    }

    @Override
    public long count() {

        return repository.count();
    }

    @Override
    //@CacheEvict(value="repoAdapterCache", allEntries = true)
    public void delete(@NonNull T dto) {

        boolean shouldDelete = entityAdapter.deleteByDto(dto);

        if(shouldDelete) {

            TID dtoId = dtoIdHelper.getId(dto);
            EID id = idAdapter.getEntityId(dtoId);

            E entity = entitiesById.get(dtoId);
            if (jdbcLogger.isInfoEnabled()) {
                jdbcLogger.info("-- " + entity.toString());
            }

            repository.delete(id);
        }
    }

    @Override
    public void delete(TID dtoId) {

        T dto = this.findOne(dtoId);

        this.delete(dto);
    }

    @Override
    public void delete(Iterable<? extends T> iterable) {

        iterable.forEach(this::delete);
    }

    @Override
    public void deleteAll() {

        repository.deleteAll();
    }
}