package com.mgiorda.repositoryadapter.support.adapter.querydsl;

import com.querydsl.core.types.Predicate;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Optional;

@Component
public class QueryDslAdapterKeyGenerator implements KeyGenerator {

    @Override
    public Object generate(Object target, Method method, Object... params) {

        String predicateKey = Optional.ofNullable(params[0])
                                      .map(param -> (Predicate) param)
                                      .map(Object::hashCode)
                                      .map(String::valueOf)
                                      .orElse("");

        String pageableKey = Optional.ofNullable(params[1])
                                    .map(param -> (Pageable) param)
                                     .map(Object::hashCode)
                                     .map(String::valueOf)
                                    .orElse("");

        return target.getClass() + predicateKey + pageableKey;
    }
}