package com.mgiorda.repositoryadapter.support.adapter.util;

import com.mgiorda.repositoryadapter.support.adapter.IdAdapter;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class CounterIdAdapter<E, ID extends Serializable> implements IdAdapter<Integer, E, ID>{

    private final Map<ID, Integer> entityIdMap = new HashMap<>();
    private final Map<Integer, ID> dtoIdMap = new HashMap<>();

    @Override
    public Integer getDtoId(E entity, ID entityId) {

        Integer id = entityIdMap.get(entityId);
        if(id == null) {

            id = (Integer.class.isAssignableFrom(entityId.getClass())) ?
                    (Integer) entityId : entityIdMap.size() + 1;

            entityIdMap.put(entityId, id);
            dtoIdMap.put(id, entityId);
        }

        return id;
    }

    @Override
    public ID getEntityId(Integer dtoId) {
        return dtoIdMap.get(dtoId);
    }
}
