package com.mgiorda.repositoryadapter.support.adapter;

public interface EntityAdapter<T, E> {

    T createDto(E entity);

    void updateDto(T dto, E entity);

    E createEntity(T dto);

    void updateEntity(E entity, T dto);

    default boolean deleteByDto(T dto) {
        return true;
    }
}
