package com.mgiorda.repositoryadapter.support.adapter.util;

import com.mgiorda.repositoryadapter.support.adapter.EntityAdapter;
import com.mgiorda.repositoryadapter.support.adapter.ListEntityAdapter;
import lombok.NonNull;
import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;
import java.util.List;

public class SimpleListEntityAdapter<T, E> extends ListEntityAdapterHelper<Object, T, Object, E> {

    public <EID extends Serializable> SimpleListEntityAdapter(@NonNull CrudRepository<E, EID> repository, @NonNull EntityAdapter<T, E> entityAdapter, Class<T> dtoType, Class<E> entityType) {
        super(repository, new ListAdapter<T, E>(entityAdapter), new CounterIdAdapter<>(), dtoType, entityType);
    }

    public List<E> updateEntityList(@NonNull List<E> entityList, @NonNull List<T> dtoList) {
        return this.updateEntityList(null, entityList, null, dtoList);
    }

    public List<T> readEntityList(@NonNull List<E> entityList) {
        return this.readEntityList(null, entityList);
    }

    public static class ListAdapter<T, E> implements ListEntityAdapter<Object, T, Object, E> {

        private final EntityAdapter<T, E> entityAdapter;

        public ListAdapter(@NonNull EntityAdapter<T, E> entityAdapter) {
            this.entityAdapter = entityAdapter;
        }

        @Override
        public E createEntity(Object baseEntity, Object baseDto, T dto) {
            return entityAdapter.createEntity(dto);
        }

        @Override
        public void updateEntity(E entity, T dto) {
            entityAdapter.updateEntity(entity, dto);
        }

        @Override
        public T createDto(Object baseEntity, E entity) {
            return entityAdapter.createDto(entity);
        }

        @Override
        public void updateDto(T dto, E entity) {
            entityAdapter.updateDto(dto, entity);
        }
    }
}
