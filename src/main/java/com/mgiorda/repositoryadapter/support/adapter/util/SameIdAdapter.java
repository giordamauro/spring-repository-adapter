package com.mgiorda.repositoryadapter.support.adapter.util;

import com.mgiorda.repositoryadapter.support.adapter.IdAdapter;

import java.io.Serializable;

public class SameIdAdapter<E, ID extends Serializable> implements IdAdapter<ID, E, ID>{

    public ID getDtoId(E entity, ID entityId) {
        return entityId;
    }

    public ID getEntityId(ID dtoId) {
        return dtoId;
    }
}
