package com.mgiorda.repositoryadapter.support.adapter.util;

import com.mgiorda.repositoryadapter.support.adapter.IdAdapter;
import com.mgiorda.repositoryadapter.support.adapter.IdReflectionHelper;
import com.mgiorda.repositoryadapter.support.adapter.ListEntityAdapter;
import lombok.NonNull;
import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ListEntityAdapterHelper<BT, T, BE, E> {

    private final IdAdapter<Serializable, E, Serializable> idAdapter;

    private final IdReflectionHelper<T, Serializable> dtoIdHelper;
    private final IdReflectionHelper<E, Serializable> entityIdHelper;

    private final CrudRepository<E, ?> repository;
    private final ListEntityAdapter<BT, T, BE, E> listEntityAdapter;

    private final Map<Serializable, T> dtosByEntityId = new HashMap<>();

    public <TID extends Serializable, EID extends Serializable> ListEntityAdapterHelper(@NonNull CrudRepository<E, EID> repository, @NonNull ListEntityAdapter<BT, T, BE, E> listEntityAdapter, @NonNull IdAdapter<TID, E, EID> idAdapter, Class<T> dtoType, Class<E> entityType) {

        this.repository = repository;
        this.listEntityAdapter = listEntityAdapter;
        this.idAdapter = (IdAdapter<Serializable, E, Serializable>) idAdapter;

        this.dtoIdHelper = new IdReflectionHelper<>(dtoType);
        this.entityIdHelper = new IdReflectionHelper<>(entityType);
    }

    public List<E> updateEntityList(BE baseEntity, @NonNull List<E> entityList, BT baseDto, @NonNull List<T> dtoList) {

        Map<Object, E> entityMap = new HashMap<>();
        entityList.forEach(entity -> {

            Object entityId = entityIdHelper.getId(entity);
            entityMap.put(entityId, entity);
        });

        List<E> result = dtoList.stream()
                .map(dto -> {

                    Object entityId = this.getEntityId(dto);
                    E entity = entityMap.get(entityId);

                    if (entity == null) {
                        entity = listEntityAdapter.createEntity(baseEntity, baseDto, dto);

                    } else {
                        entityMap.remove(entityId);
                    }

                    listEntityAdapter.updateEntity(entity, dto);

                    return entity;
                })
                .collect(Collectors.toList());

        repository.delete(entityMap.values());

        return result;
    }

    public List<T> readEntityList(BE baseEntity, @NonNull List<E> entityList) {

        return entityList.stream()
                .map(entity ->  this.readEntity(baseEntity, entity))
                .collect(Collectors.toList());
    }

    private T readEntity(BE baseEntity, E entity) {

        Serializable entityId = entityIdHelper.getId(entity);

        T dto = dtosByEntityId.get(entityId);
        if(dto == null) {

            dto = listEntityAdapter.createDto(baseEntity, entity);
            Serializable dtoId = idAdapter.getDtoId(entity, entityId);

            dtoIdHelper.setId(dto, dtoId);
            dtosByEntityId.put(entityId, dto);
        }

        listEntityAdapter.updateDto(dto, entity);

        return dto;
    }

    public Object getEntityId(T dto) {

        Serializable dtoId = dtoIdHelper.getId(dto);
        return idAdapter.getEntityId(dtoId);
    }
}
