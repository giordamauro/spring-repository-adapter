package com.mgiorda.repositoryadapter.adapted.impl;

import com.mgiorda.repositoryadapter.app.model.UserGroup;
import com.mgiorda.repositoryadapter.app.repository.UserGroupRepository;
import com.mgiorda.repositoryadapter.adapted.model.GroupType;
import com.mgiorda.repositoryadapter.adapted.model.NewGroup;
import com.mgiorda.repositoryadapter.adapted.repository.NewGroupRepository;
import com.mgiorda.repositoryadapter.support.adapter.EntityAdapter;
import com.mgiorda.repositoryadapter.support.adapter.IdAdapter;
import com.mgiorda.repositoryadapter.support.adapter.querydsl.QueryDslMappingsAdapter;
import com.mgiorda.repositoryadapter.support.adapter.querydsl.QueryDslRepositoryAdapterSupport;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;
import java.util.function.Function;

@Component
public class NewGroupRepositoryImpl extends QueryDslRepositoryAdapterSupport<NewGroup, String, UserGroup, Integer> implements NewGroupRepository {

    @Autowired
    public NewGroupRepositoryImpl(UserGroupRepository repository) {
        super(repository, new NewGroupEntityAdapter(), new NewGroupNaturalIdAdapter(repository));

        this.setMappingsAdapter(new NewGroupMappingsAdapter());
    }

    public static class NewGroupMappingsAdapter implements QueryDslMappingsAdapter {

        @Override
        public Map<String, Function<String, String>> getKeyMappings() {
            return Collections.singletonMap("groupType", key -> "type");
        }

        @Override
        public Map<String, Function<Object, Object>> getValueMappings() {
            return Collections.singletonMap("groupType", value -> ((GroupType) value).getValue());
        }
    }

    public static class NewGroupEntityAdapter implements EntityAdapter<NewGroup, UserGroup> {

        @Override
        public UserGroup createEntity(NewGroup dto) {

            return new UserGroup();
        }

        @Override
        public void updateEntity(UserGroup entity, NewGroup dto) {

            entity.setName(dto.getName());
            entity.setType(dto.getGroupType()
                              .getValue());
        }

        @Override
        public NewGroup createDto(UserGroup entity) {

            return new NewGroup(entity.getName());
        }

        @Override
        public void updateDto(NewGroup dto, UserGroup entity) {

            dto.setGroupType(GroupType.valueOfCode(entity.getType()));
        }
    }

    public static class NewGroupNaturalIdAdapter implements IdAdapter<String, UserGroup, Integer> {

        private final UserGroupRepository repository;

        public NewGroupNaturalIdAdapter(@NonNull UserGroupRepository repository) {
            this.repository = repository;
        }

        @Override
        public String getDtoId(UserGroup entity, Integer entityId) {
            return entity.getName();
        }

        @Override
        public Integer getEntityId(String dtoId) {

            Integer testId = 123;
            return repository.findIdByNameAndCompanyId(dtoId, testId);
        }
    }
}