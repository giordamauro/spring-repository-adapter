package com.mgiorda.repositoryadapter.adapted.impl;

import com.mgiorda.repositoryadapter.app.model.Company;
import com.mgiorda.repositoryadapter.app.repository.CompanyRepository;
import com.mgiorda.repositoryadapter.adapted.model.NewCompany;
import com.mgiorda.repositoryadapter.adapted.repository.NewCompanyRepository;
import com.mgiorda.repositoryadapter.support.adapter.EntityAdapter;
import com.mgiorda.repositoryadapter.support.adapter.IdAdapter;
import com.mgiorda.repositoryadapter.support.adapter.querydsl.QueryDslRepositoryAdapterSupport;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NewCompanyRepositoryImpl extends QueryDslRepositoryAdapterSupport<NewCompany, String, Company, Integer> implements NewCompanyRepository {

    @Autowired
    public NewCompanyRepositoryImpl(CompanyRepository repository) {
        super(repository, new CompanyEntityAdapter(), new CompanyNaturalIdAdapter(repository));
    }

    public static class CompanyEntityAdapter implements EntityAdapter<NewCompany, Company> {

        @Override
        public Company createEntity(NewCompany dto) {

            Company entity = new Company();
            entity.setName(dto.getName());

            return entity;
        }

        @Override
        public void updateEntity(Company entity, NewCompany dto) {

            entity.setName(dto.getName());
            entity.setDisplayName(dto.getDisplayName());
        }

        @Override
        public NewCompany createDto(Company entity) {

            return new NewCompany(entity.getName());
        }

        @Override
        public void updateDto(NewCompany dto, Company entity) {

            dto.setDisplayName(entity.getDisplayName());
        }
    }

    public static class CompanyNaturalIdAdapter implements IdAdapter<String, Company, Integer> {

        private final CompanyRepository repository;

        public CompanyNaturalIdAdapter(@NonNull CompanyRepository repository) {
            this.repository = repository;
        }

        @Override
        public String getDtoId(Company entity, Integer entityId) {
            return entity.getName();
        }

        @Override
        public Integer getEntityId(String dtoId) {

            return repository.findIdByName(dtoId);
        }
    }
}