package com.mgiorda.repositoryadapter.adapted.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)

@Entity
public class NewGroup {

    @Id
    private final String name;

    private GroupType groupType = GroupType.ACCESS;

    public NewGroup(@NonNull String name) {
        this.name = name;
    }
}