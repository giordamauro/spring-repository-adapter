package com.mgiorda.repositoryadapter.adapted.model;

import lombok.Getter;
import lombok.NonNull;

public enum GroupType {

    ACCESS("Access"),
    INTERNAL("Internal"),
    BUDGET("Budget"),
    SECTOR("Sector"),
    TEST("Test");

    @Getter
    private final String value;

    GroupType(String value) {
        this.value = value;
    }

    public static GroupType valueOfCode(@NonNull String code) {

        for (GroupType groupType : GroupType.values()) {

            if(groupType.getValue().equalsIgnoreCase(code)){
                return groupType;
            }
        }
        throw new IllegalArgumentException("GroupType not found for code: " + code);
    }
}