package com.mgiorda.repositoryadapter.adapted.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)

@Entity
public class NewCompany {

    @Id
    private final String name;

    @NotNull
    private String displayName;

    public NewCompany(@NonNull String name){

        this.name = name;
        this.displayName = name;
    }
}
