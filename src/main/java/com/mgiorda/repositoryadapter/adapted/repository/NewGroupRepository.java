package com.mgiorda.repositoryadapter.adapted.repository;

import com.mgiorda.repositoryadapter.adapted.model.NewGroup;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface NewGroupRepository extends PagingAndSortingRepository<NewGroup, String>, QueryDslPredicateExecutor<NewGroup> {
}
