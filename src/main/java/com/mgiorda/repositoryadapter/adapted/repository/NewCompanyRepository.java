package com.mgiorda.repositoryadapter.adapted.repository;

import com.mgiorda.repositoryadapter.adapted.model.NewCompany;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface NewCompanyRepository extends PagingAndSortingRepository<NewCompany, String>, QueryDslPredicateExecutor<NewCompany> {
}
